import yaml
import numpy as np
import csv


class Objekt:
    def __init__(self, name, wbw, rw=0, nd=0, mCPY=100000, erloes=1, material=0, fertigung=0, zinsen=0, abschreibung=0):
        self.name = name
        self.wbw = wbw
        self.rw = rw
        self.nd = nd
        self.sonstigeKosten = []
        self.maxCapacityPerYear = mCPY
        self.erloes = erloes
        self.fertigung = fertigung
        self.material = material
        self.kalkAbsch = abschreibung
        self.kalkZinsen = 0
        self.kFix = 0
        self.kVar = 0
        self.zinsen = zinsen

    def appendCost(self, val):
        self.sonstigeKosten.append(val)

    def getSonstigeCostKum(self):
        val = 0.0
        for vals in self.sonstigeKosten:
            val += vals
        return val

    def calcKalkAbsch(self):
        self.kalkAbsch = (self.wbw - self.rw) / self.nd

    def calcKalkZinsen(self):
        self.kalkZinsen = self.getdKaptitalBindung() * self.zinsen

    def getdKaptitalBindung(self):
        return (self.wbw + self.rw) / 2

    def calcKFix(self):

        if self.nd > 0:
            self.calcKalkAbsch()

        if self.zinsen > 0:
            self.calcKalkZinsen()

        self.kFix = self.kalkAbsch + self.kalkZinsen + self.getSonstigeCostKum()

    def calcKVar(self):
        if self.fertigung > 0 or self.material > 0:
            self.kVar = (self.fertigung + self.material) * \
                self.maxCapacityPerYear

    def getKosten(self):
        self.calcKFix()
        self.calcKVar()

        return np.round(self.kFix + self.kVar, 2)

    def getKostenProStueck(self):
        return np.round(self.getKosten() / self.maxCapacityPerYear, 2)

    def getGewinn(self):
        return np.round(self.erloes * self.maxCapacityPerYear - self.getKosten(), 2)

    def getGewinnProStueck(self):
        return np.round(self.getGewinn() / self.maxCapacityPerYear, 2)

    def getRentabilitaet(self, zinsen=0):
        return np.round((self.getGewinn() + zinsen) / self.getdKaptitalBindung() * 100, 2)


ymlData = []

name = "kvgl.yml"
with open(name) as file:
    cfg = yaml.safe_load(file)
    for data in cfg["data"]:
        obj = Objekt(data["name"], data["kosten"])

        try:
            obj.restwert = data["restwert"]
        except:
            pass

        try:
            obj.nd = data["nutzungsdauer"]
        except:
            pass

        try:
            obj.maxCapacityPerYear = data["maxCap"]
        except:
            pass

        try:
            obj.erloes = data["erloesProStueck"]
        except:
            pass

        try:
            obj.material = data["material"]
        except:
            pass

        try:
            obj.fertigung = data["fertigung"]
        except:
            pass

        try:
            obj.zinsen = data["zinsen"]
        except:
            pass

        try:
            obj.kalkAbsch = data["abschreibung"]
        except:
            pass

        try:
            for k in data["sonstigeKosten"]:
                obj.appendCost(k)
        except:
            pass

        ymlData.append(obj)

for d in ymlData:
    print("=================================")
    print("\t", d.name)
    print()
    print("Kosten:", d.getKosten())
    print("Kosten Pro Stück:", d.getKostenProStueck())
    print("Gewinn:", d.getGewinn())
    print("Gewinn Pro Stueck:", d.getGewinnProStueck())
    print("Rentabilität:", d.getRentabilitaet())
    print()

with open("kostenvglfin.csv", "w",  newline="") as csvfile:
    writer = csv.writer(csvfile, delimiter=";")

    writer.writerow(["Name", "Kosten", "Kosten pro Stueck", "Gewinn",
                     "Gewinn pro Stueck", "Rentabilitaet"])

    for d in ymlData:
        writer.writerow([d.name, d.getKosten(), d.getKostenProStueck(
        ), d.getGewinn(), d.getGewinnProStueck(), d.getRentabilitaet()])
