# Scripts for EWI

## ABC Analyse

Die Daten zum Auswerten müssen als CSV Datei angegeben werden. Dabei ist das Schema: _NAME;MENGE;PREIS_

```csv
Produkt 1;620;5,00
Produkt 2;1050;1,20
Produkt 3;400;3,40
Produkt 4;655;2,40
```

Nachdem das Programm mit `python abcAnalyse.py` ausgeführt wurde, wurde eine Datei `abcfin.csv` erzeugt, welche das Ergebnis der ABC Analyse bereit hält.

## Kapitalwertmethode

Hier muss eine txt-Datei mit dem Namen `kapdata.txt` gegeben sein, welche als Inhalt alle Werte mit einem Semicolon getrennt enthält.

```txt
-25000;18000;17000
```

Das Program wird mit `python Kapitalwert.py` gestartet und gibt das Ergebnis in der Konsole preis.

## Kostenvergleich, Gewinnvergleich, Rentabilitätsvergleich

Zuerst muss eine yml Datei mit der benötigten Alternativen Konfiguration erstellt werden: `kvgl.yml`. Diese sollte folgendem Schema entsprechen. Null werte können auch weggelassen werden.

```yml
data:
  - name: Alternative A
    kosten: 250000
    restwert: 0
    nutzungsdauer: 0
    maxCap: 200000
    erloesProStueck: 1.2
    material: 0
    fertigung: 0
    zinsen: 0
    abschreibung: 25000
    sonstigeKosten:
      - 35000
      - 80000
  - name: Alternative B
    kosten: 330000
    maxCap: 250000
    erloesProStueck: 0.95
    sonstigeKosten:
      - 37000
      - 95000
    abschreibung: 33000
```

Das ganze mit Python ausführen `python Kostenvgl.py`. Die Daten können in der Konsole ausgewertet werden, oder auch mit dem erzeugte CSV File weiterverarbeitet werden (`kostenvglfin.csv`).
