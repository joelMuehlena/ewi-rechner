import csv
import numpy as np

name = "abcdata.csv"
output = "abcfin.csv"
maxA = 80
maxB = 95
maxC = 100

#name = input("Bitte datei namen angeben: ")
#output = input("Bitte datei namen für output angeben: ")
#maxA = input("Bitte Grenze für Gruppe A angeben: ")
#maxB = input("Bitte Grenze für Gruppe B angeben: ")
#maxC = input("Bitte Grenze für Gruppe C angeben: ")

sumOfValue = 0.0

prods = []
prodsSortedPercent = []


class Produkt:
    def __init__(self, name, count, value):
        self.name = name
        self.value = value
        self.count = count
        self.group = ""

    def getUmsatz(self):
        return self.value * self.count

    def getUmsatzInPercent(self):
        return np.round(self.getUmsatz() / sumOfValue * 100, 2)


with open(name) as csvdata:
    csv_reader = csv.reader(csvdata, delimiter=";")
    for row in csv_reader:
        value = np.round(float(row[2].replace(",", ".")), 2)
        prod = Produkt(row[0], np.round(
            float(row[1].replace(",", ".")), 2), value)
        prods.append(prod)
        sumOfValue += prod.getUmsatz()

prodsSortedPercent = sorted(
    prods, key=lambda x: x.getUmsatzInPercent(), reverse=True)

sumOfPercent = 0.0
for prod in prodsSortedPercent:
    sumOfPercent += prod.getUmsatzInPercent()
    if sumOfPercent < maxA:
        prod.group = "A"
    elif sumOfPercent < maxB:
        prod.group = "B"
    else:
        prod.group = "C"

with open(output, "w", newline="") as csvfile:
    writer = csv.writer(csvfile, delimiter=";")
    writer.writerow(["Name", "Anzahl", "Kosten", "Umsatz", "Umsatz in %", "Nach size in %",
                     "% kummuliert", "Gruppe ( " + str(maxA) + " / " + str(maxB) + " / " + str(maxC) + ")"])

    index = 0
    sumOfPercent = 0.0
    for prod in prods:
        sumOfPercent += prodsSortedPercent[index].getUmsatzInPercent()
        writer.writerow([prod.name, prod.count, str(prod.value) + " EUR", str(prod.getUmsatz()) + " EUR", prod.getUmsatzInPercent(),
                         prodsSortedPercent[index].name, np.round(sumOfPercent, 2), prodsSortedPercent[index].group])
        index += 1

print("Success")
