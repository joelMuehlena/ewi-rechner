name = "kapdata.txt"

#name = input("Bitte datei namen angeben: ")


data = []

with open(name, "r") as file:
    text = file.read()
    data = text.split(";")


def calcVal(val, percent, exponent):
    return val / (1 + percent) ** exponent


sum = 0.0
percentFix = 0.08

for i in range(len(data)):
    sum += calcVal(float(data[i]), percentFix, i)

print("Summe:", sum)

if sum > 0:
    print("Projekt kann durchgeführt werden")
else:
    print("Projekt sollte nicht durchgeführt werden")
